#!/usr/bin/env python3
# -- coding: utf-8 --
# adapted from https://gist.github.com/ckolumbus/10103544 and others (see below)
# by Nicolas Geldner

# severe lack of documentation for pdfminer, unfortunately

import sys
import os
import fnmatch
import codecs
import re
import copy
import argparse
import csv
from lib.misc_helper import *
import lib.tree_processing_parsing as tree_proc
import lib.text_processing as text_proc

arg_parser = argparse.ArgumentParser(
        description="Extract achieved points from commented student exercise submissions in PDF format")
arg_parser.add_argument("directory",
                        help="Directory containing student submissions, structured like an "
                             "extracted ILIAS batch download")
arg_parser.add_argument("worksheet_file", metavar="worksheet",
                        help="Solutionless worksheet file to reference for available points")
arg_parser.add_argument("-d", "--debug", action="store_true", help="show debug output from the lexer")
arg_parser.add_argument("-g", "--mci-groupex", action="store_true", help="use given submissions to evaluate (only) MCI group exercises; usually only individual exercises are evaluated")

args = arg_parser.parse_args()

# also checks for solutions and throws an error if found
try:
    parse_result = tree_proc.ParseTree(args.worksheet_file, args.debug, args.mci_groupex)
except AssertionError as e:
    print(str(e) + " (file: " + args.worksheet_file + ")", file=sys.stderr)
    sys.exit(1)
except IOError as e:
    print(str(e) + " (file: " + args.worksheet_file + ")", file=sys.stderr)
    sys.exit(1)

print("Achievable:")
print(parse_result)

try:
    dirlist = sorted(os.walk(args.directory))
except IOError as e:
    print(str(e), file=sys.stderr)
    sys.exit(1)
# skip to top level directories
try:
    root, toplevel_dirnames, filenames = dirlist[1]
except IndexError:
    print(error_string() + "No subdirectories found. Exiting.", file=sys.stderr)
    sys.exit(1)

for dirname in toplevel_dirnames:
    if re.match("[A-Z][^_]*_[A-Z][^_]*_[A-Z][^_]*[A-Z][^_]*_[1-9][0-9]{5}", dirname) is None:
        print(warning_string() + "Subdirectory does not conform to ILIAS naming " \
                                                "rules (" + dirname + ")\n", file=sys.stderr)

some_files_found = False

tsv_file = codecs.open(os.path.join(args.directory, "points-" + os.path.split(args.worksheet_file[:-4])[-1] + ".tsv"),
                       "w")
tsv_out = csv.writer(tsv_file, dialect="excel-tab")
tsv_out.writerow(["Uploader", "Percentage"] + parse_result.get_name_list())
achievable_points = parse_result.get_point_list()
tsv_out.writerow(["", ""] + achievable_points)

comments_per_exercise = {}

for root, dirnames, filenames in dirlist[1:]:
    if root.count(os.path.sep) > 1: # TODO fix/flexibility
        print(warning_string() + "Subdirectory nesting detected (directory: " + \
                             root + ")\n", file=sys.stderr)

    files = fnmatch.filter(filenames, '*.pdf')
    uploader = os.path.split(root)[-1]

    if len(files) == 0:
        print(warning_string() + "Subdirectory contains no PDF files (" + root + ")\n", file=sys.stderr)
    elif len(files) > 1:
        print(error_string() + "Subdirectory contains more than one PDF file (" + root + ")\n", file=sys.stderr)
    else:
        filename = files[0]
        some_files_found = True

        outfile = codecs.open(os.path.join(root, "-".join(
                ["points", list(parse_result.keys())[0], os.path.split(root)[-1]]) + ".txt"), "w")
        outfile.write("Uploader: " + uploader + "\n\n")

        file_path = os.path.join(root, filename)
        personal_parse_tree = copy.deepcopy(parse_result)
        deductions_total = []
        comments = text_proc.extract_pdf_comments(file_path)
        if len(comments) == 0:
            print(warning_string() + "No comments found in PDF file (file: " + filename + ")\n", file=sys.stderr)

        for comment in comments:
            deductions_this_comment, deductionless_lines_this_comment = text_proc.extract_deductions(comment)
            if len(deductionless_lines_this_comment) > 0:
                print(warning_string() + "Comment contains lines with no point " \
                                                        "deductions (lines: \"" + "\", \"".join(
                        deductionless_lines_this_comment) + "\" (file: " + filename + ")\n", file=sys.stderr)
            deductions_total += deductions_this_comment

        for descend_list, deducted_points, comment_line in sorted(deductions_total):
            try:
                personal_parse_tree.deduct(descend_list,
                                           deducted_points)
            except AssertionError as e:
                print(str(e) + " (exercise: " + ".".join(
                        descend_list) + ") (file: " + filename + ")", file=sys.stderr)
                continue

            outfile.write(str(deducted_points) + " on exercise " + ".".join(
                    descend_list) + ":" + comment_line + "\n")

            overview_text = "(" + str(deducted_points) + ")" + comment_line + "\n"
            try:
                comments_per_exercise[" --- " + ".".join(
                        descend_list) + " --- "].append(overview_text)
            except KeyError:
                comments_per_exercise[" --- " + ".".join(
                        descend_list) + " --- "] = [overview_text]

        outfile.write("\nResult:\n")
        outfile.write(str(personal_parse_tree))

        point_list = personal_parse_tree.get_point_list()
        # format as percentage with two decimal points
        tsv_out.writerow([uploader, "{:.2%}".format(point_list[0] / achievable_points[0])] + point_list)

        outfile.close()

        # add user info to file name
        if re.findall("_feedback", filename) == []:
            os.rename(file_path, file_path[:-4] + "_feedback.pdf")
            file_path = file_path[:-4] + "_feedback.pdf"
        if re.findall("_" + uploader, filename) == []:
            os.rename(file_path, file_path[:-4] + "_" + uploader + ".pdf")

print("TSV overview written to " + tsv_file.name)
tsv_file.close()

comment_file = codecs.open(os.path.join(args.directory, "comments-" + os.path.split(args.worksheet_file[:-4])[-1] + ".txt"),
                           "w")
for key in sorted(comments_per_exercise):
    comment_file.write("\n" + key + "\n")
    for comment in comments_per_exercise[key]:
        comment_file.write(comment)

print("Comment overview written to " + comment_file.name + "\n")
comment_file.close()

if not some_files_found:
    print(error_string() + "No PDF files found. Exiting.", file=sys.stderr)
    sys.exit(1)
