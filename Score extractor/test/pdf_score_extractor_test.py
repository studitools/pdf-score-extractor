#!/usr/bin/env python3
# -- coding: utf-8 --
import unittest
import lib.tree_processing_parsing as tree_proc
import lib.text_processing as text_proc
from lib.misc_helper import *
from os import path

__author__ = 'geldnens'

# intended tests:
# application of correct comments to the correct exercises,
# subexercises, subsubexercises; actual warning/error cases >= intended warning/error cases

ro_test_tree = {
    "Aufgabenblatt 90": {
        "points": 25,
        "children": {
            "1": {"points": 2, "children": {"a": {"points": 2, "children": {}}}},
            "2": {
                "points": 18,
                "children": {
                    "a": {"points": 4, "children": {}},
                    "b": {"points": 2, "children": {}},
                    "c": {"points": 5, "children": {}},
                    "d": {"points": 1, "children": {}},
                    "e": {"points": 6, "children": {}}
                }
            },
            "3": {
                "points": 5,
                "children": {}
            }
        }
    }
}

skp_test_tree = {
    "Aufgabenblatt 90": {
        "points": 26,
        "children": {
            "1": {
                "points": 10,
                "children": {
                    "a": {"points": 2, "children": {}},
                    "b": {
                        "points": 8,
                        "children": {
                            "i"   : {"points": 2, "children": {}},
                            "ii"  : {"points": 2, "children": {}},
                            "iii" : {"points": 2, "children": {}},
                            "iv"  : {"points": 2, "children": {}}
                        }
                    }
                }
            },
            "2": {
                "points": 11,
                "children": {
                    "a": {"points": 4, "children": {"i": {"points": -1, "children": {}}}},
                    "b": {"points": 2, "children": {"i": {"points":  2, "children": {}}}},
                    "c": {
                        "points": 5,
                        "children": {
                            "i":  {"points": -1, "children": {}},
                            "ii": {"points": -1, "children": {}}
                        }
                    }
                }
            },
            "3": {
                "points": 5,
                "children": {}
            }
        }
    }
}
                
mci_test_tree_individual = {
    'Übung / Tutorial 1 (individual)': {
        'points': 1.0,
        'children': {
            'A': {
                'points': 1.0,
                'children': {
                    'A.1': {'points': 0.5,  'children': {}},
                    'A.2': {'points': 0.25, 'children': {}},
                    'A.3': {'points': 0.25, 'children': {}}
                },
            }
        }
    }
}

mci_test_tree_group = {
    'Übung / Tutorial 1 (group)': {
        'points': 2.0,
        'children': {
            'B': {
                'points': 2.0,
                'children': {
                    'B.1': {'points': 1.0, 'children': {}},
                    'B.2': {'points': 1.0, 'children': {}}
                }
            }
        }
    }
}

class TestPDFParsing(unittest.TestCase):
    """
    Test creation of parse trees from existing PDFs, as well as comment parsing
    """

    def test_ro_parsing(self):
        self.maxDiff = None
        tree = tree_proc.ParseTree(path.join("test","ro_test.pdf"))
        self.assertEqual(tree, ro_test_tree)

    def test_skp_parsing(self):
        self.maxDiff = None
        tree = tree_proc.ParseTree(path.join("test","skp_test.pdf"))
        self.assertEqual(tree, skp_test_tree)
        
    def test_mci_parsing_individual(self):
        self.maxDiff = None
        tree = tree_proc.ParseTree(path.join("test","mci_test.pdf"))
        self.assertEqual(tree, mci_test_tree_individual)

    def test_mci_parsing_group(self):
        self.maxDiff = None
        tree = tree_proc.ParseTree(path.join("test","mci_test.pdf"), mci_groupex=True)
        self.assertEqual(tree, mci_test_tree_group)

    def test_comment_parsing(self):
        comments = text_proc.extract_pdf_comments(path.join("test","blank_commented_test.pdf"))
        self.maxDiff = None
        self.assertEqual(comments, ["test1\ntest2", "test3"])


class TestPointDeduction(unittest.TestCase):
    """
    Test application of deductions to a parse tree.
    """

    def setUp(self):
        # RO tree has fewer features
        self.tree_to_process = tree_proc.ParseTree(skp_test_tree)

    def tearDown(self):
        del self.tree_to_process

    def test_multiple_deducting(self):
        """
        Ensure executing more than one deduction on the same data works.
        :return: nothing
        """
        self.tree_to_process.deduct(["1", "b", "iv"], -2)
        self.tree_to_process.deduct(["3"], -2)

        self.assertEqual(self.tree_to_process, {
            "Aufgabenblatt 90": {
                "points": 22,
                "children": {
                    "1": {
                        "points": 8,
                        "children": {
                            "a": {"points": 2, "children": {}},
                            "b": {
                                "points": 6,
                                "children": {
                                    "i": {"points": 2, "children": {}},
                                    "ii": {"points": 2, "children": {}},
                                    "iii": {"points": 2, "children": {}},
                                    "iv": {"points": 0, "children": {}}}}}},
                    "2": {
                        "points": 11,
                        "children": {
                            "a": {"points": 4, "children": {"i": {"points": -1, "children": {}}}},
                            "b": {"points": 2, "children": {"i": {"points": 2, "children": {}}}},
                            "c": {
                                "points": 5,
                                "children": {
                                    "i": {"points": -1, "children": {}},
                                    "ii": {"points": -1, "children": {}}}}}},
                    "3": {
                        "points": 3,
                        "children": {}}}}})

    def test_zeroing(self):
        """
        Ensure the point values of lower-level exercise parts are set to -2 (invalidated, -1 = originally invalid) when
        a deduction is applied to a higher-level exercise part on the same path
        :return: nothing
        """
        self.maxDiff = None
        self.tree_to_process.deduct(["1"], -2)
        self.assertEqual(self.tree_to_process, {
            "Aufgabenblatt 90": {
                "points": 24,
                "children": {
                    "1": {
                        "points": 8,
                        "children": {
                            "a": {"points": -1, "children": {}},
                            "b": {
                                "points": -1,
                                "children": {
                                    "i": {"points": -1, "children": {}},
                                    "ii": {"points": -1, "children": {}},
                                    "iii": {"points": -1, "children": {}},
                                    "iv": {"points": -1, "children": {}}}}}},
                    "2": {
                        "points": 11,
                        "children": {
                            "a": {"points": 4, "children": {"i": {"points": -1, "children": {}}}},
                            "b": {"points": 2, "children": {"i": {"points": 2, "children": {}}}},
                            "c": {
                                "points": 5,
                                "children": {
                                    "i": {"points": -1, "children": {}},
                                    "ii": {"points": -1, "children": {}}}}}},
                    "3": {
                        "points": 5,
                        "children": {}}}}})

    def test_deduction_with_subsubexercise_points(self):
        """
        Check if points are correctly deducted, down to the lowest level, if the lowest level
        carries a valid point number.
        :return: nothing
        """
        self.tree_to_process.deduct(["1", "b", "i"], -2)
        self.maxDiff = None
        self.assertEqual(self.tree_to_process, {
            "Aufgabenblatt 90": {
                "points": 24,
                "children": {
                    "1": {
                        "points": 8,
                        "children": {
                            "a": {"points": 2, "children": {}},
                            "b": {
                                "points": 6,
                                "children": {
                                    "i": {"points": 0, "children": {}},
                                    "ii": {"points": 2, "children": {}},
                                    "iii": {"points": 2, "children": {}},
                                    "iv": {"points": 2, "children": {}}}}}},
                    "2": {
                        "points": 11,
                        "children": {
                            "a": {"points": 4, "children": {"i": {"points": -1, "children": {}}}},
                            "b": {"points": 2, "children": {"i": {"points": 2, "children": {}}}},
                            "c": {
                                "points": 5,
                                "children": {
                                    "i": {"points": -1, "children": {}},
                                    "ii": {"points": -1, "children": {}}}}}},
                    "3": {
                        "points": 5,
                        "children": {}}}}})

    def test_deduction_without_subsubexercise_points(self):
        """
        Check if points are correctly deducted, down to the lowest level, if the lowest level
        does not carry a valid point number.
        :return: nothing
        """
        self.maxDiff = None

        self.tree_to_process.deduct(["2", "a", "i"], -2)
        self.assertEqual(self.tree_to_process, {
            "Aufgabenblatt 90": {
                "points": 24,
                "children": {
                    "1": {
                        "points": 10,
                        "children": {
                            "a": {"points": 2, "children": {}},
                            "b": {
                                "points": 8,
                                "children": {
                                    "i": {"points": 2, "children": {}},
                                    "ii": {"points": 2, "children": {}},
                                    "iii": {"points": 2, "children": {}},
                                    "iv": {"points": 2, "children": {}}}}}},
                    "2": {
                        "points": 9,
                        "children": {
                            "a": {"points": 2, "children": {"i": {"points": -1, "children": {}}}},
                            "b": {"points": 2, "children": {"i": {"points": 2, "children": {}}}},
                            "c": {
                                "points": 5,
                                "children": {
                                    "i": {"points": -1, "children": {}},
                                    "ii": {"points": -1, "children": {}}}}}},
                    "3": {
                        "points": 5,
                        "children": {}}}}})

    def test_nonexistence_recovery(self):
        """
        Check if attempts to deduct from a nonexistent exercise part are detected.
        :return: nothing
        """
        try:
            self.tree_to_process.deduct(["3", "a", "i"], -2)
        except AssertionError as e:
            self.assertEqual(str(e), error_string() + "Referenced exercise part does not exist.")
        else:
            self.fail("Nonexistence detection failed")
        finally:
            self.assertEqual(self.tree_to_process, skp_test_tree)

    def test_overdeduction_recovery(self):
        """
        Check if attempts to deduct more points from an exercise part than achievable are
        detected.
        :return:
        """
        try:
            self.tree_to_process.deduct(["1", "b"], -10)
        except AssertionError as e:
            self.assertEqual(str(e),
                             error_string() + "More points deducted than remaining in exercise "
                                              "part. (10 > 8)")
        else:
            self.fail("Overdeduction detection failed")
        finally:
            # rollback
            self.assertEqual(self.tree_to_process, skp_test_tree)


class TestCommentProcessing(unittest.TestCase):
    """
    Test parsing of deductions from a PDF comment.
    """

    def setUp(self):
        self.comment_text = "[1.a | -1.0] test1\n\n[2.b.iv | -3] test2\n[3 | -1.5] test3\nsomething else\n[A.1 | -0.25] stuff\n[B | -1] more"

    def tearDown(self):
        del self.comment_text

    def test_deduction_output(self):
        deductions, _ = text_proc.extract_deductions(self.comment_text)
        # descend_list
        self.assertEqual(sorted([deduction[0] for deduction in deductions]), [["1", "a"], ["2", "b", "iv"], ["3"], ["A", "1"], ["B"]])
        # deduct
        self.assertEqual(sorted([deduction[1] for deduction in deductions]), [-3.0, -1.5, -1.0, -1, -0.25])
        # comment
        self.assertEqual(sorted([deduction[2] for deduction in deductions]), [' more', ' stuff', ' test1', ' test2', ' test3'])

    def test_deductionless_line_output(self):
        _, lines = text_proc.extract_deductions(self.comment_text)
        self.assertEqual(lines, {"", "something else"})


class TestTreeOutput(unittest.TestCase):
    """
    Test stringification of parse trees.
    """

    def setUp(self):
        self.simple_test_tree = tree_proc.ParseTree({"Blatt": {
            "points": 18, "children": {
                "1": {
                    "points": 8,
                    "children": {
                        "a": {
                            "points": 5,
                            "children": {
                                "i": {
                                    "points": -1,
                                    "children": {}
                                },
                                "ii": {
                                    "points": -1,
                                    "children": {}
                                }
                            }
                        },
                        "b": {
                            "points": 3,
                            "children": {
                                "i": {
                                    "points": 0,
                                    "children": {}
                                },
                                "ii": {
                                    "points": 3,
                                    "children": {}
                                }
                            }
                        }
                    }
                }, "2": {
                    "points": 10,
                    "children": {}
                }
            }
        }})

    def tearDown(self):
        del self.simple_test_tree

    def test_tree_output_text(self):
        out = self.simple_test_tree.__str__()
        self.assertEqual(out,
                          "Blatt: 18 points\n    1: 8 points\n        a: 5 points\n        b: 3 points\n            ii: 3 points\n    2: 10 points\n")


if __name__ == '__main__':
    unittest.main()
