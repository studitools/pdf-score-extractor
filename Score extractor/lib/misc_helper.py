# -- coding: utf-8 --
import colorama
__author__ = 'geldnens'


def error_string():
    return colorama.Fore.RED + "Error: " + colorama.Style.RESET_ALL


def warning_string():
    return colorama.Fore.YELLOW + "Warning: " + colorama.Style.RESET_ALL

NO_POINT_NO = -1


