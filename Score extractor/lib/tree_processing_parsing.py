# -- coding: utf-8 --
from ply import lex, yacc
import copy
from re import match
from .misc_helper import *
from .text_processing import extract_pdf_text
import queue

__author__ = 'geldnens'

# works on SKP and RO worksheets
tokens = (
    "WORKSHEET_NAME", "MCI_WORKSHEET_NAME", "MAX_SCORE", "MCI_MAX_SCORE", "RO_WORKSHEET_MAX", "EXERCISE_NUMBER", "SUBEXERCISE_NUMBER", "SUBSUBEXERCISE_NUMBER", 
    "EXERCISE_LETTER", "SUBEXERCISE_NUMBER_MCI", 
    "RO_SUBEXERCISE_POINTS", "SKP_SUBEXERCISE_POINTS", "MCI_EXERCISE_POINTS", "MCI_EXERCISE_INDIVIDUAL", "MCI_EXERCISE_GROUP",
    "SOLUTION_MARKER", "OPTIONAL_EXERCISE")
    

def level(points, children, type=None):
    new_level = { "points": points, "children": children }
    if type:
        new_level["type"] = type # "individual" or "group" for MCI sheet exercise, else empty

    return new_level


# http://stackoverflow.com/questions/38987/how-can-i-merge-two-python-dictionaries-in-a-single
# -expression
def merge_dicts(x, y):
    """Given two dicts, merge them into a new dict as a shallow copy."""
    z = x.copy()
    z.update(y)
    return z

# helper function that allows doing arithmetic on values that might not exist
def none_to_nil(none_or_value):
    return none_or_value if none_or_value else 0


# tokens for lex

def t_WORKSHEET_NAME(t):
    r"(?m)^Aufgabenblatt\s+[1-9][0-9]?"
    return t
    
def t_MCI_WORKSHEET_NAME(t):
    r"Tutorial\s+[1-9]" # only matches this?
    return t    


def t_SOLUTION_MARKER(t):
    r"Musterlösung|Solution:"
    raise AssertionError(
            error_string() + "Only solutionless PDFs can currently be parsed. Exiting.")


def t_MAX_SCORE(t):
    r"(?s)[1-9][0-9]?\s+(Punkt(e)?|point(s)?)\)?$"
    t.value = int(t.value[0:2])
    return t

def t_MCI_MAX_SCORE(t):
    r"\([1-9][0-9]?\s+Punkte\s+/\s+Points\)$"
    t.value = float(t.value[1:3])
    return t


def t_RO_WORKSHEET_MAX(t):
    r"Gesamtpunktzahl\s+Blatt:\s+[1-9][0-9]?"
    t.value = int(t.value[-2:])
    return t


def t_RO_SUBEXERCISE_POINTS(t):
    r"\[[1-9][0-9]?P\]"
    t.value = int(match(r"\[([1-9][0-9]?)P\]", t.value).group(1))
    return t


def t_SKP_SUBEXERCISE_POINTS(t):
    r"\[[1-9][0-9]?\s+(Zusatzpunkt(e?)|Punkt(e)?|point(s)?)\]"
    t.value = int(match(r"\[([1-9][0-9]?)\s+", t.value).group(1))
    return t


def t_MCI_EXERCISE_POINTS(t):
    # sometimes erroneous comma or lack of spaces
    r"(?m)[a-zA-Z]*\([0-9]((\.|,)(2|7)?5)?\s+Punkt(e)?\s+/\s+Point(s)?\)"    
    t.value = float(match(r"[a-zA-Z]*\(([0-9]((\.|,)(2|7)?5)?)", t.value).group(1).replace(",","."))
    return t


def t_MCI_EXERCISE_INDIVIDUAL(t):
    r"Einzelaufgabe\s+/\s+Individual\s+exercise\!"
    t.value = "individual"
    return t


def t_MCI_EXERCISE_GROUP(t):
    r"Gruppenaufgabe\s+/\s+Group\s+exercise\!"
    t.value = "group"
    return t
 


# skip everything irrelevant
# http://stackoverflow.com/questions/5022129/ply-lex-parsing-problem
def t_error(t):
    t.lexer.skip(1)


# (hopefully) ignore occurrences that simply reference the exercise by only choosing occurences at the beginning of a line
# requires the "multiline" modifier
def t_EXERCISE_NUMBER(t):
    r"(?m)^Aufgabe\s+[1-9](\:|\s+)"
    t.value = t.value[-2]
    return t


# should be safe enough
def t_SUBEXERCISE_NUMBER(t):
    r"(?m)^\([a-z]\)(\s+|\n)"
    t.value = t.value[1:2]
    return t


def t_SUBSUBEXERCISE_NUMBER(t):
    r"(?m)^i(v|ii?)?\.\s+"
    # any easy, better way?
    t.value = match(r"(?m)^(i(v|ii?)?)\.\s+", t.value).group(1)
    return t


def t_OPTIONAL_EXERCISE(t):
    r"(?m)^Zusatzaufgabe"
    return t
    
def t_EXERCISE_LETTER(t):
    r"(?m)^[A-Z]\)\s+"
    t.value = t.value[0:1]
    return t
    
def t_SUBEXERCISE_NUMBER_MCI(t):
    r"(?m)\n\n[A-Z]\.[1-9]\)\s+" # double newline to be safe
    t.value = t.value[-3]
    return t


# rules for yacc, including some consistency checking
# builds a parse tree, see the test file for an example

def p_start(p):
    '''start : ro_start
             | skp_start
             | mci_start'''
    p[0] = p[1]


def p_ro_start(p):
    '''ro_start : WORKSHEET_NAME overall_info RO_WORKSHEET_MAX'''
    if p[2]["points"] != p[3]:
        raise AssertionError(
                error_string() + "Exercise points don't add up to worksheet points. (sheet: " + str(
                        p[3]) + " != exes: " + str(p[2]["points"]) + ") Exiting. (parsing " + p[1] + ")")
    p[0] = {p[1]: p[2]}


def p_skp_start(p):
    '''skp_start : WORKSHEET_NAME MAX_SCORE overall_info'''
    if p[2] != p[3]["points"]:
        raise AssertionError(
                error_string() + "Exercise points don't add up to worksheet points. Exiting. (sheet: "
                                 "" + str(p[2]) + " != exes: " + str(p[3]["points"]) + ") (parsing " +
                p[1] + ")")
    p[0] = {p[1]: p[3]}

def p_mci_start(p):
    '''mci_start : MCI_WORKSHEET_NAME MCI_MAX_SCORE overall_info'''
    if p[2] != p[3]["points"]:
        raise AssertionError(
                error_string() + "Exercise points don't add up to worksheet points. Exiting. (sheet: "
                                 "" + str(p[2]) + " != exes: " + str(p[3]["points"]) + ") (parsing " +
                p[1] + ")")
    p[0] = {p[1]: p[3]}


def p_overall_info_comb(p):
    '''overall_info : exercise_info overall_info'''
    p1_values = next(iter(p[1].values())) # need to skip past the exercise name

    p[0] = level(p1_values["points"] + p[2]["points"],
                 merge_dicts(p[1], p[2]["children"]))
    for type in ["individual", "group"]:
        p[0][type] = (p1_values["points"] if type == p1_values.get("type") else 0) \
                    + none_to_nil(p[2].get(type)) # watch parens!
    

def p_overall_info_single(p):
    '''overall_info : exercise_info'''
    p1_values = next(iter(p[1].values())) # need to skip past the exercise name

    p[0] = level(p1_values["points"], p[1])
    for type in ["individual", "group"]:
        p[0][type] = p1_values["points"] if type == p1_values.get("type") else 0


# no need to invest a lot of effort here
def p_optional_exercise_discard(p):
    '''overall_info : OPTIONAL_EXERCISE subexercise_info'''
    p[0] = level(0, {})


def p_exercise_info_comb(p):
    '''exercise_info : EXERCISE_NUMBER MAX_SCORE subexercise_info
                       | EXERCISE_LETTER MCI_EXERCISE_POINTS MCI_EXERCISE_INDIVIDUAL subexercise_info
                       | EXERCISE_LETTER MCI_EXERCISE_POINTS MCI_EXERCISE_GROUP subexercise_info'''
    i = 3
    if len(p) > 4: # MCI
        i += 1
        p[i]["type"] = p[3]
    
    if p[2] != p[i]["points"]:
        raise AssertionError(
                error_string() + "Subexercise points don't add up to exercise points. Exiting. (ex: "
                + str(
                        p[2]) + " != subexes: " + str(p[i]["points"]) + ") (parsing exercise " + p[1] + ")")
    p[0] = {p[1]: p[i]}


def p_exercise_info_single(p):
    '''exercise_info : EXERCISE_NUMBER MAX_SCORE
                       | EXERCISE_LETTER MCI_EXERCISE_POINTS MCI_EXERCISE_INDIVIDUAL
                       | EXERCISE_LETTER MCI_EXERCISE_POINTS MCI_EXERCISE_GROUP'''
    if len(p) > 3:
        p[0] = {p[1]: level(p[2], {}, p[3])}
    else:
        p[0] = {p[1]: level(p[2], {})}


# Needs a bunch of rules to discard subexercise names. Not including single subexes makes things
# easier...
def p_subex_name_span_comb(p):
    '''subex_name_span : SUBEXERCISE_NUMBER subex_name_span'''


def p_subex_name_span_single(p):
    '''subex_name_span : SUBEXERCISE_NUMBER'''


def p_subexercise_info_ro_comb(p):
    '''subexercise_info : subex_name_span RO_SUBEXERCISE_POINTS subexercise_info'''

    index = 'a'
    cur_subex = {index: level(p[2], {})}
    orig_keys = sorted(list(p[3]["children"].keys()), reverse=True)

    # reassigns letters at each step because they're usually all over the place in the generated text
    for key in orig_keys:
        p[3]["children"][chr(ord(key) + 1)] = copy.deepcopy(p[3]["children"][key])
        del p[3]["children"][key]

    p[0] = level(p[2] + p[3]["points"], merge_dicts(cur_subex, p[3]["children"]))


def p_subexercise_info_ro_comb_error(p):
    '''subexercise_info : RO_SUBEXERCISE_POINTS subexercise_info'''

    index = 'a'
    cur_subex = {index: level(p[1], {})}
    orig_keys = sorted(list(p[2]["children"].keys()), reverse=True)

    # moves exercise names
    for key in orig_keys:
        p[2]["children"][chr(ord(key) + 1)] = copy.deepcopy(p[2]["children"][key])
        del p[2]["children"][key]

    p[0] = level(p[1] + p[2]["points"], merge_dicts(cur_subex, p[2]["children"]))


def p_subexercise_info_ro_single1(p):
    '''subexercise_info : RO_SUBEXERCISE_POINTS'''
    cur_subex = {'a': level(p[1], {})}
    p[0] = level(p[1], cur_subex)


def p_subexercise_info_ro_single2(p):
    '''subexercise_info : RO_SUBEXERCISE_POINTS subex_name_span'''
    cur_subex = {'a': level(p[1], {})}
    p[0] = level(p[1], cur_subex)


def p_subexercise_info_ro_single3(p):
    '''subexercise_info : subex_name_span RO_SUBEXERCISE_POINTS'''
    cur_subex = {'a': level(p[2], {})}
    p[0] = level(p[2], cur_subex)


def p_subexercise_info_comb(p):
    '''subexercise_info : SUBEXERCISE_NUMBER SKP_SUBEXERCISE_POINTS subexercise_info
                           | SUBEXERCISE_NUMBER_MCI MCI_EXERCISE_POINTS subexercise_info'''
    cur_subex = {p[1]: level(p[2], {})}
    p[0] = level(p[2] + p[3]["points"], merge_dicts(cur_subex, p[3]["children"]))


def p_subexercise_info_comb_sub(p):
    '''subexercise_info : SUBEXERCISE_NUMBER subsubexercise_info subexercise_info'''
    cur_subex = {p[1]: level(p[2]["points"], p[2]["children"])}
    p[0] = level(p[2]["points"] + p[3]["points"], merge_dicts(cur_subex, p[3]["children"]))


def p_subexercise_info_comb_pathsub(p):
    '''subexercise_info : SUBEXERCISE_NUMBER SKP_SUBEXERCISE_POINTS subsubexercise_info_no_points subexercise_info'''
    cur_subex = {p[1]: level(p[2], p[3]["children"])}
    p[0] = level(p[2] + p[4]["points"], merge_dicts(cur_subex, p[4]["children"]))


def p_subexercise_info_single(p):
    '''subexercise_info : SUBEXERCISE_NUMBER SKP_SUBEXERCISE_POINTS
                           | SUBEXERCISE_NUMBER_MCI MCI_EXERCISE_POINTS'''
    cur_subex = {p[1]: level(p[2], {})}
    p[0] = level(p[2], cur_subex)


def p_subexercise_info_single_sub(p):
    '''subexercise_info : SUBEXERCISE_NUMBER subsubexercise_info'''
    cur_subex = {p[1]: level(p[2]["points"], p[2]["children"])}
    p[0] = level(p[2]["points"], cur_subex)


def p_subexercise_info_single_pathsub(p):
    '''subexercise_info : SUBEXERCISE_NUMBER SKP_SUBEXERCISE_POINTS subsubexercise_info_no_points'''
    cur_subex = {p[1]: level(p[2], p[3]["children"])}
    p[0] = level(p[2], cur_subex)


def p_subsubexercise_info_no_points_comb(p):
    '''subsubexercise_info_no_points : SUBSUBEXERCISE_NUMBER subsubexercise_info_no_points'''
    cur_subsubex = {p[1]: level(-1, {})}
    p[0] = level(0, merge_dicts(cur_subsubex, p[2]["children"]))


def p_subsubexercise_info_no_points_single(p):
    '''subsubexercise_info_no_points : SUBSUBEXERCISE_NUMBER'''
    cur_subsubex = {p[1]: level(-1, {})}
    p[0] = level(0, cur_subsubex)


def p_subsubexercise_info_comb(p):
    '''subsubexercise_info : SUBSUBEXERCISE_NUMBER SKP_SUBEXERCISE_POINTS subsubexercise_info'''
    cur_subsubex = {p[1]: level(p[2], {})}
    p[0] = level(p[2] + p[3]["points"], merge_dicts(cur_subsubex, p[3]["children"]))


def p_subsubexercise_info_single(p):
    '''subsubexercise_info :  SUBSUBEXERCISE_NUMBER SKP_SUBEXERCISE_POINTS'''
    cur_subsubex = {p[1]: level(p[2], {})}
    p[0] = level(p[2], cur_subsubex)


def p_error(p):
    # None = EOF reached
    if p is None:
        raise AssertionError(
                error_string() + "PDF does not appear to be a RO or SKP worksheet. Exiting.")
    else:
        raise AssertionError(
                error_string() + "Parsing token failed (token value: " + str(p.value) + ")")


class ParseTree(dict):
    def __init__(self, pdf_path_or_dict, debug=False, mci_groupex=False):
        """
        Generate a parse tree from a PDF or an existing dict.
        :param pdf_path_or_dict: The path to the PDF to parse, or a dict to initialize with.
        :param debug: Whether to produce debug output.
        """
        if type(pdf_path_or_dict) is str:
            lexer = lex.lex()
            text = extract_pdf_text(pdf_path_or_dict)

            if debug:
                lexer.input(text)
                for token in lexer:
                    print(token)
            parser = yacc.yacc()
            tree = parser.parse(text)
            super(ParseTree, self).__init__(tree)
        else:
            super(ParseTree, self).__init__(copy.deepcopy(pdf_path_or_dict))


        top_level = next(iter(self.values()))

        if none_to_nil(top_level.get("individual")) > 0 or none_to_nil(top_level.get("group")) > 0:
            # MCI, prune either individual or group exercises/points
            del_type = "group"
            keep_type = "individual"
            if mci_groupex:
                del_type = "individual"
                keep_type = "group"

            to_delete = [] # need to do this as dict is not allowed to change size during iteration
            for exercise in top_level["children"]:
                if top_level["children"][exercise]["type"] == del_type:
                    to_delete.append(exercise)
            for exercise in to_delete:
                del top_level["children"][exercise]
            for exercise in top_level["children"]:
                del top_level["children"][exercise]["type"]
                
            top_level["points"] = top_level[keep_type]

            # change sheet title to reflect processing
            sheet_name = next(iter(self.keys()))
            self[sheet_name + " (" + keep_type + ")"] = self[sheet_name]
            del self[sheet_name]

        if top_level.get("group") is not None:
            del top_level["group"]
        if top_level.get("individual") is not None:
            del top_level["individual"]
        

    def __str__(self):
        """
        Convert the parse tree to a human-readable string.
        :param cutoff_depth: The maximum depth of items to process (0 for no maximum)
        """
        ret = ""

        for name, points, depth in self.walk():
            ret += depth * "    " + name + ": " + str(
                    points) + " points\n" if points > 0 else ""
        return ret

    def walk(self):
        """
        Coroutine/iterator to perform a preorder walk of the parse tree.
        :param cutoff_depth: The maximum depth of exercise parts to return
        :return: at each vertex/exercise part encountered, its name, points and depth in the tree
        """
        subtrees = queue.LifoQueue()
        subtrees.put((self, 0))

        while not subtrees.empty():
            cur = subtrees.get()
            cur_name = list(cur[0].keys())[0]
            cur_points = cur[0][cur_name]["points"]
            yield (cur_name, cur_points, cur[1])
            for key, value in sorted(list(cur[0][cur_name]["children"].items()), reverse=True):
                subtrees.put(({key: value}, cur[1] + 1))

    def get_point_list(self):
        """
        Get the point numbers noted in this parse tree.
        :return: a list of float or integer values representing the achieved points
        """
        point_list = []
        for _, points, _ in self.walk():
            point_list.append(points if points != NO_POINT_NO else "N/A")
        return point_list

    def get_name_list(self):
        """
        Get the exercise names/numbers noted in this parse tree
        :return: a list of strings containing the names/numbers
        """
        name_list = []
        for name, _, _ in self.walk():
            name_list.append(name)
        return name_list

    def deduct(self, descend_via, points_to_deduct):
        """
        Apply a deduction to a parse tree at each applicable level.
        :param descend_via: The ordered list of names comprising the name of the exercise to deduct
        from.
        :param points_to_deduct: The point value to deduct (negative sign)
        """

        def deduct_helper(top_copy, unset_nodes=False):
            """ Helper function to perform the actual deduction """
            if unset_nodes:
                top_copy["points"] = NO_POINT_NO

            elif top_copy["points"] > 0:
                # node is not unset
                if top_copy["points"] + points_to_deduct < 0:
                    raise AssertionError(
                            error_string() + "More points deducted than remaining in exercise part. (" +
                            str(
                                    -points_to_deduct) + " > " + str(top_copy["points"]) + ")")
                else:
                    top_copy["points"] += points_to_deduct

        start = copy.deepcopy(self)
        current = start[list(start.keys())[0]]


        assert points_to_deduct <= 0
        deduct_helper(current)

        past = []
        while len(descend_via) != 0:
            next_child = descend_via[0]
            past.append(next_child)

            descend_via = descend_via[1:]

            try:
                current = current["children"][next_child]
            except KeyError as e:
                # lower part nonexistent
                raise AssertionError(error_string() + "Referenced exercise part does not exist.")

            try:
                deduct_helper(current)
            except AssertionError:
                # overdeduction
                raise

        # beim untersten zu betrachtenden Aufgabenteil angelangt, weiter unten alles auf "nicht
        # spezifiziert" setzen
        # level-order iteration
        processing_queue = queue.Queue()
        processing_queue.put(current)

        while not processing_queue.empty():
            to_unset = processing_queue.get()
            for child in list(to_unset["children"].keys()):
                deduct_helper(to_unset["children"][child], True)
                processing_queue.put(to_unset["children"][child])

        self.__init__(start)
