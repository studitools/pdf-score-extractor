# -- coding: utf-8 --
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdftypes import PDFObjectNotFound
from io import StringIO
import sys
import re

__author__ = 'geldnens'


# http://stackoverflow.com/questions/26494211/extracting-text-from-a-pdf-file-using-pdfminer-in
# -python
def extract_pdf_text(path):
    """
    Extract text from a given PDF file.
    :param path: the PDF file to extract the text from
    :return: the text extracted from the PDF (should be the same as the output of pdf2txt.py)
    """
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, laparams=laparams)
    fp = open(path, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0
    caching = True
    pagenos = set()

    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password,
                                  caching=caching, check_extractable=True):
        interpreter.process_page(page)

    text = retstr.getvalue()

    fp.close()
    device.close()
    retstr.close()
    return text

# with visualization help from regex101.com
# TODO: use a (nicer?) parser for this instead of a concatenated bunch of regexes? Also: detect
# potential input errors?

# deducted points, such as: -10.5, -0.0, -3
points_re = r"(-[0-9]{1,2}(\.(0|5|25|75))?)"
# ugly magic numbers when using regexes
points_re_group = 11

ex_re = r"([1-9]|[A-Z])(\)|\.)?"
ex_name_re_group = 1

subex_re = r"([a-zA-Z]|[1-9])(\)|\.)?"
subex_name_re_group = 4

# only goes up to iv
subsubex_re = r"(i(v|ii?)?)(\)|\.)?"
subsubex_name_re_group = 7

deduction_comment_re = re.compile(
    r"(\[\s*" + ex_re + r"\s*(" + subex_re + r"(\s*" + subsubex_re + r")?\s*)?(:|\|)\s*" + points_re +
    r"\]([^\n]*))(\n|$)")
comment_re_group = 14


def extract_deductions(comment_text):
    """
    Extract/parse point deductions specified in the given PDF comment text. They must be
    specified in the format given in the regex above (which allows several variations), and there
    may be only one deduction in each line of the comment. Deductions may be specified per
    exercise, subexercise and subsubexercise as applicable.

    Examples:
        [1|-10] Missing.
        [1.a: -0.5] Needs some polish.
        [1.b.i | -0] Tiny error that doesn't even matter.

    :param comment_text: The text of the PDF comment to parse deductions from.
    :return: A list of the deductions parsed from this PDF comment. A deduction is represented as
    a tuple of the (ordered) part names that form the exercise part to be deducted from,
    the amount of points to deduct, and the rest of the comment line this deduction was
    parsed from (for informational purposes),
    """
    global deduction_comment_re

    # less awkward than finditer iterator for emptiness checking
    matches = set(match for match in deduction_comment_re.findall(comment_text))
    match_lines = set(match[0] for match in matches)

    deductions = []

    for match in matches:
        ex_name = match[ex_name_re_group]
        subex_name = match[subex_name_re_group]
        if subex_name != "":
            subex_name = subex_name.lower()
        subsubex_name = match[subsubex_name_re_group]

        deduct = float(match[points_re_group])

        descend_list = [ex_name]
        if subex_name != "":
            descend_list.append(subex_name)
            if subsubex_name != "":
                descend_list.append(subsubex_name)
        deductions.append((descend_list, deduct, match[comment_re_group]))

    lines = set(comment_text.split("\n"))
    # return lines containing no deductions
    return deductions, lines - match_lines


def extract_pdf_comments(pdf_file_path):
    """
    Extract the text of all comments added to a PDF file.
    :param pdf_file_path: The PDF file to extract the comments from
    :return: A list of strings containing the text of all comments found.
    """
    comments = []

    fp = open(pdf_file_path, 'rb')
    parser = PDFParser(fp)
    doc = PDFDocument(parser)
    visited = set()
    for xref in doc.xrefs:
        for objid in xref.get_objids():
            if objid in visited: continue
            visited.add(objid)
            try:
                obj = doc.getobj(objid)
                if obj is None:
                    continue

                if isinstance(obj, dict) and 'C' in obj and 'Contents' in obj:
                    # sanitize output; pdfminer apparently uses latin-1 exclusively
                    comments.append(obj['Contents'].decode('latin-1').replace("\xfe\xff", "").replace("\x00", ""))
            except PDFObjectNotFound as e:
                print('not found: %r' % e, file=sys.stderr)
    fp.close()
    return comments
