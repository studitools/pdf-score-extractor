# PDF score extractor

Parses RO, SKP, MCI worksheets (Univ. Stgt., may be necessary to rearrange the point numbers on the former) and commented exercise submissions, outputs an overview of point deductions for each submitter (to be uploaded as a feedback file), a TSV overview of points achieved by the exercise group and a textual overview of deduction comments for each exercise.

## Installation

`pip install pdfminer ply` and clone the repository to wherever you want it. (pdfminer must be newer than late 2013)

## Usage

 1. Download submission ZIP from ILIAS and extract it anywhere
 2. Make sure you have a copy of the matching worksheet (without solutions) available
 3. Add comments containing point deductions to the PDFs (example: "[1.b.i | -1] very wrong", "[2 | -20] missing"), at most one deduction per comment line
 4. pdf_score_extractor.py DIRECTORY WORKSHEET (check output for errors)
 5. Re-compress directory and upload it using the ILIAS "multi-feedback" functionality, mark total points in ILIAS
 6. Peruse the output

## License

MIT, if you need one...
